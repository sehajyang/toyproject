package com.kitri.yunjey.model;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.kitri.yunjey.vo.Board;
import com.kitri.yunjey.vo.pageBean;

@Component("BoardService")
public class BoardService implements BoardMapper {
@Resource(name="sqlSession")
private SqlSession sqlSession;
private BoardMapper mapper;

    @Override
    public pageBean<Board> boardcount() {
        mapper = sqlSession.getMapper(BoardMapper.class);
        return mapper.boardcount();
    }
    
	@Override
	public ArrayList<Board> boardselect(pageBean<Board> pagebean) {
	    mapper = sqlSession.getMapper(BoardMapper.class);
		return mapper.boardselect(pagebean);
	}
	
    @Override
    public ArrayList<Board> searchselect(pageBean<Board> pagebean) {
        mapper = sqlSession.getMapper(BoardMapper.class);
        return mapper.searchselect(pagebean);
    }
	
	@Override
	public Board boardselect2(int num) {
		mapper = sqlSession.getMapper(BoardMapper.class);
		System.out.println("num"+num);
		return mapper.boardselect2(num);
	}

	@Override
	public void listinsert(Board board) {
		mapper =sqlSession.getMapper(BoardMapper.class);
		mapper.listinsert(board);
		  /*MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)req;
		    Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
		    MultipartFile multipartFile = null;
		    while(iterator.hasNext()){
		        multipartFile = multipartHttpServletRequest.getFile(iterator.next());
		        if(multipartFile.isEmpty() == false){
		            System.out.println(multipartFile.getName());
		            System.out.println(multipartFile.getOriginalFilename());
		            System.out.println(multipartFile.getSize());
		        }
		    }*/
	}

	@Override
	public void listupdate(Board board) {
		mapper =sqlSession.getMapper(BoardMapper.class);
		mapper.listupdate(board);
	}

	@Override
	public void listdelete(int num) {
		mapper =sqlSession.getMapper(BoardMapper.class);
		mapper.listdelete(num);
		
	}



}
