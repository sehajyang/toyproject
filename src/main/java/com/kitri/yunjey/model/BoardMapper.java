package com.kitri.yunjey.model;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.kitri.yunjey.vo.Board;
import com.kitri.yunjey.vo.pageBean;

public interface BoardMapper {
    pageBean<Board> boardcount();
	ArrayList<Board> boardselect(pageBean<Board> pagebean);
	ArrayList<Board> searchselect(pageBean<Board> pagebean);
	Board boardselect2(int numm);
	void listinsert(Board board);
	void listupdate(Board board);
	void listdelete(int num);
}
