package com.kitri.yunjey;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.kitri.yunjey.model.BoardMapper;
import com.kitri.yunjey.vo.Board;
import com.kitri.yunjey.vo.pageBean;

@Controller
public class HomeController {
    @Resource(name = "BoardService")
    BoardMapper service;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home(HttpServletRequest req) {
        ModelAndView mv = new ModelAndView("home");
        return mv;
    }

    @RequestMapping(value = "/board", method = RequestMethod.GET)
    public ModelAndView board(HttpServletRequest req, @RequestParam("page") String page) {
        ModelAndView mv = new ModelAndView("board");
        String searchtext = req.getParameter("data");

        ArrayList<Board> list = null;
        pageBean<Board> pb = service.boardcount();

        // 페이징
        int totalPage = 0;
        int intPage = Integer.parseInt(page);
        int cntPerPage = 5;// 1페이지별 5건씩 보여준다
        totalPage = (int) Math.ceil((double) pb.getTotalCount() / cntPerPage);
        int cntPerPageGroup = 5; // 페이지그룹별 5페이지씩 보여준다
        int startPage = (int) Math.floor((double) (intPage) / (cntPerPageGroup + 1)) * cntPerPageGroup + 1;
        int endPage = startPage + cntPerPageGroup - 1;
        int endRow = cntPerPage * intPage;
        int startRow = endRow - cntPerPage;

        if (endPage > totalPage) {
            endPage = totalPage;
        }
        pb.setCurrentPage(intPage);// 현재페이지
        pb.setTotalPage(totalPage); // 총페이지
        pb.setStartPage(startPage); // 시작페이지
        pb.setEndPage(endPage); // 끝페이지
        pb.setTotalCount(pb.getTotalCount()); // 총 게시글 갯수
        pb.setCntPerPage(cntPerPage);
        pb.setStartRow(startRow);
        pb.setEndRow(endRow);
        if (searchtext != null) {
            pb.setSearchtext(searchtext);
            list = service.searchselect(pb);
            if (list.size() == 0) {
                mv.addObject("nulltext", "0");
                System.out.println("nulllll" + list);
            }
        } else {
            list = service.boardselect(pb);
            System.out.println("blist" + list);
        }
        pb.setList(list); // 목록
        mv.addObject("pageBean", pb);
        mv.addObject("nulltext", "1");
        return mv;
    }

    @RequestMapping(value = "/writeview", method = RequestMethod.GET)
    public String writeview(Model model, HttpServletRequest req) {
        model.addAttribute("mode", req.getParameter("mode"));
        model.addAttribute("docNum", req.getParameter("docNum"));
        System.out.println(req.getParameter("mode"));
        System.out.println(req.getParameter("docNum"));
        return "write";
    }

    @RequestMapping(value = "/write*", method = RequestMethod.POST)
    public String write(HttpServletRequest req, @ModelAttribute Board board) throws Exception {
        System.out.println("write 들어옴");
        /* 파일 업로드 */
        MultipartFile files = board.getFiles();
        String fileName = files.getOriginalFilename();
        String path = "C:\\upload\\" + fileName;
        File f = new File(path);
        try {
            files.transferTo(f);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /* 평범한 insert */
        board.setWriter("admin");
        board.setCmtYn("0");
        board.setPdocNum(0);

        if (req.getParameter("doc_num") != null) {
            board.setPdocNum(Integer.parseInt(req.getParameter("doc_num")));
        }
        service.listinsert(board);
        return"redirect:/board?page=1";
        /*return "submissionComplete";*/
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView view(HttpServletRequest req, @RequestParam("num") String num) throws Exception {
        ModelAndView mv = new ModelAndView("view");

        int numm = Integer.parseInt(num);
        Board list = service.boardselect2(numm);

        mv.addObject("list", list);

        return mv;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView edit(HttpServletRequest req, @ModelAttribute Board board, @RequestParam("num") String num)
            throws Exception {
        ModelAndView mv = new ModelAndView();
        int numm = Integer.parseInt(num);

        if (board.getContents() == null) {
            Board list = service.boardselect2(numm);
            mv.addObject("list", list);
            mv.setViewName("edit");
        } else {
            board.setDocNum(numm);
            service.listupdate(board);
            mv.setViewName("redirect:/board?page=1");
        }
        return mv;
    }

    @RequestMapping(value = "/del", method = RequestMethod.GET)
    public String del(HttpServletRequest req, @RequestParam("num") int num) throws Exception {
        service.listdelete(num);
        return "redirect:/board?page=1";
    }

}
