package com.kitri.yunjey.vo;

import java.util.List;

public class pageBean<T> {
    private int currentPage;
    private int totalPage;
    private List<T> list;
    private int startPage;
    private int endPage;
    private int totalCount;  //총 게시물 갯수
    private int cntPerPage;  //한 페이지당 보여줄 갯수
    private int startRow;
    private int endRow;
    private String searchtext;

    public pageBean() {
        super();
    }

   
    public pageBean(int currentPage, int totalPage, List<T> list, int startPage, int endPage, int totalCount,
            int cntPerPage, int startRow, int endRow, String searchtext) {
        super();
        this.currentPage = currentPage;
        this.totalPage = totalPage;
        this.list = list;
        this.startPage = startPage;
        this.endPage = endPage;
        this.totalCount = totalCount;
        this.cntPerPage = cntPerPage;
        this.startRow = startRow;
        this.endRow = endRow;
        this.searchtext = searchtext;
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getCntPerPage() {
        return cntPerPage;
    }

    public void setCntPerPage(int cntPerPage) {
        this.cntPerPage = cntPerPage;
    }

    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    
    public String getSearchtext() {
        return searchtext;
    }


    public void setSearchtext(String searchtext) {
        this.searchtext = searchtext;
    }


    @Override
    public String toString() {
        return "pageBean [currentPage=" + currentPage + ", totalPage=" + totalPage + ", list=" + list + ", startPage="
                + startPage + ", endPage=" + endPage + ", totalCount=" + totalCount + ", cntPerPage=" + cntPerPage
                + ", startRow=" + startRow + ", endRow=" + endRow + ", searchtext=" + searchtext + "]";
    }
   
}
