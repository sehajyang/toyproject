package com.kitri.yunjey.vo;

public class Users {   
    private Integer userNum;
    private String  id;
    private String  pwd;
    private String  userName;
    private String  nicname;
    private String  email;
    private String  major;
    
    public Users() {
        super();
    }
    public Users(Integer userNum, String id, String pwd, String userName, String nicname, String email, String major) {
        super();
        this.userNum = userNum;
        this.id = id;
        this.pwd = pwd;
        this.userName = userName;
        this.nicname = nicname;
        this.email = email;
        this.major = major;
    }
    public Integer getUserNum() {
        return userNum;
    }
    public void setUserNum(Integer userNum) {
        this.userNum = userNum;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getPwd() {
        return pwd;
    }
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
   
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getNicname() {
        return nicname;
    }
    public void setNicname(String nicname) {
        this.nicname = nicname;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getMajor() {
        return major;
    }
    public void setMajor(String major) {
        this.major = major;
    }
    
    
}
