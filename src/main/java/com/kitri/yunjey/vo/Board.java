package com.kitri.yunjey.vo;

import org.springframework.web.multipart.MultipartFile;

public class Board {
    private int docNum;
    private int pdocNum;
    private String docKind;
    private String writer;
    private String title;
    private String wDate;
    private String eDate;
    private String contents;
    private String cmtYn;
    private String searchtext;
    private MultipartFile files;

    public Board() {
        super();
    }

    public Board(int docNum, int pdocNum, String docKind, String writer, String title, String wDate, String eDate,
            String contents, String cmtYn, String searchtext, MultipartFile files) {
        super();
        this.docNum = docNum;
        this.pdocNum = pdocNum;
        this.docKind = docKind;
        this.writer = writer;
        this.title = title;
        this.wDate = wDate;
        this.eDate = eDate;
        this.contents = contents;
        this.cmtYn = cmtYn;
        this.searchtext = searchtext;
        this.files = files;
    }

    public int getPdocNum() {
        return pdocNum;
    }

    public void setPdocNum(int pdocNum) {
        this.pdocNum = pdocNum;
    }

    public String getSearchtext() {
        return searchtext;
    }

    public void setSearchtext(String searchtext) {
        this.searchtext = searchtext;
    }

    public int getDocNum() {
        return docNum;
    }

    public void setDocNum(int docNum) {
        this.docNum = docNum;
    }

    public String getDocKind() {
        return docKind;
    }

    public void setDocKind(String docKind) {
        this.docKind = docKind;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getwDate() {
        return wDate;
    }

    public void setwDate(String wDate) {
        this.wDate = wDate;
    }

    public String geteDate() {
        return eDate;
    }

    public void seteDate(String eDate) {
        this.eDate = eDate;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getCmtYn() {
        return cmtYn;
    }

    public void setCmtYn(String cmtYn) {
        this.cmtYn = cmtYn;
    }

    public MultipartFile getFiles() {
        return files;
    }

    public void setFiles(MultipartFile files) {
        this.files = files;
    }

    @Override
    public String toString() {
        return "Board [docNum=" + docNum + ", pdocNum=" + pdocNum + ", docKind=" + docKind + ", writer=" + writer
                + ", title=" + title + ", wDate=" + wDate + ", eDate=" + eDate + ", contents=" + contents + ", cmtYn="
                + cmtYn + ", searchtext=" + searchtext + ", files=" + files + "]";
    }

}