<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<table>
<tr>
<td>num: ${list.docNum }</td>
<td>writer: ${list.writer }</td>
<td>date: ${list.wDate }</td>
</tr>
</table>
<form method ="post" action="${pageContext.request.contextPath}/edit?num=${list.docNum}">
title:<input type="text" name="title" value="${list.title}"><br />
contents:<textarea name="contents">${list.contents}</textarea>

<input type="submit" value="글수정">
</form>
<button class = "delbtn" onclick="location.href='${pageContext.request.contextPath}/del?num=${list.docNum}'">삭제</button>
<button onclick="goBack()">뒤로가기</button>
<script>
$('.delbtn').click(function() {
		alert('삭제했습니다.');
	});
<!-- 뒤로가기 -->
function goBack() {
    window.history.back();
}
</script>
</body>
</html>