<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Insert title here</title>
</head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<body>
<h1>aaa${mode} <br /> 글번호 ${docNum }</h1>
<c:choose>
    <c:when test="${docNum eq null}">
    	<form id="frm" name="frm" method = "post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/write?mode=${mode}">
    </c:when>
    <c:otherwise>
    	<form id="frm" name="frm" method = "post" enctype="multipart/form-data" action="${pageContext.request.contextPath}/write?mode=${mode}&doc_num=${docNum}">
    </c:otherwise>
</c:choose>
	제목<input type = "text" class="title" name = "title">
	내용<textarea name = "contents" class="contents"></textarea>
	<input type = "submit" class="submitbt" value = "작성하기" >
	<input type="file" name="files">
</form>
<button onclick="location.href='${pageContext.request.contextPath}/board?page=1'">글목록</button>
<script>
	$('.submitbt').click(function() {
		if($('.title').val() == ""){
		alert('제목을 작성해주세요');
		return false;
	}else{
		alert('작성했습니다');
	}
	});
	

</script>
</body>
</html>