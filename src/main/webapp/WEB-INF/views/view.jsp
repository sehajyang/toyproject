<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<style>
table, tr, td {
 border: 1px solid black;
}

div {
border: 2px solid black;
width : 30%;
height: 150px;
}
</style>
<body>
<form method="post" action="${pageContext.request.contextPath}/edit?num=${list.docNum}">
<table>
<tr>
<td>num: ${list.docNum }</td>
<td>writer: ${list.writer }</td>
<td>date: ${list.wDate }</td>
</tr>
</table>
제목: ${list.title}
<div>
글 내용: ${list.contents}
</div>
<input type="submit" value="글수정" >
</form>
<button onclick="location.href='${pageContext.request.contextPath}/writeview?mode=dap&docNum=${list.docNum}'">답글하기</button>
<button onclick="location.href='${pageContext.request.contextPath}/board?page=1'">글목록</button>
</body>
</html>