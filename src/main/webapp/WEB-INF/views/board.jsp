<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- 페이징 관련 데이터들 -->
<c:set var="pb" value="${pageBean}" />
<c:set var="totalCount" value="${pb.totalCount}" />
<c:set var="currentPage" value="${pb.currentPage}" />
<c:set var="cntPerPage" value="${pb.cntPerPage}" />
<c:set var="startPage" value="${pb.startPage}" />
<c:set var="endPage" value="${pb.endPage}" />
<c:set var="nulltext" value="${nulltext}" />
<html>
<head>
<title>Home</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>
.page-item {
display: inline-block;
}
</style>
</head>
<body>
	<div id = "hometext">
	<h1>Hello world!</h1>
	</div>
	<input type="text" class="searchtext">
	<button class="searchbtn">검색</button>
	<table>
		<thead>
			<tr>
				<td>글번호</td>
				<td>부모글번호</td>
				<td>작성자</td>
				<td>제목</td>
				<td>작성일</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="list" items="${pb.list}" begin="0" end="4">
				<tr>
					<td>${list.docNum}</td>
					<td>${list.pdocNum}</td>
					<td>${list.writer}</td>
					<c:choose>
					    <c:when test="${list.pdocNum eq '0'}">
					    	<td><a href="${pageContext.request.contextPath}/view?num=${list.docNum}">${list.title}</a></td>
					    </c:when>
					    <c:otherwise>
					    	<td>&nbsp;답글:<a href="${pageContext.request.contextPath}/view?num=${list.docNum}">${list.title}</a></td>
					    </c:otherwise>
					</c:choose>
					<td>${list.wDate}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<!--페이징 -->
	<c:set var="startPage" value="${pb.startPage}" />
	<c:set var="endPage" value="${pb.endPage}" />
	<ul class="pagination">
		<li class="page-item"><a class="page-link" href="#"
			aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
		</a></li>
		<c:forEach begin="${pb.startPage}" end="${pb.endPage}" var="i">
			<li class="page-item"><a class="page-link" href="#">${i}</a></li>
		</c:forEach>

		<li class="page-item"><a class="page-link" href="#"
			aria-label="Next"> <span aria-hidden="true">&raquo;</span>
		</a></li>
	</ul>
	<!--글쓰기 -->
	<button
		onclick="location.href='${pageContext.request.contextPath}/writeview'">write</button>
		
	<script>	
	<!--페이징 -->
	$(function() {
	<c:if test="${nulltext eq '0'}">
		alert('존재하지 않는 글입니다 글 목록으로 돌아갑니다 ');
		location.href = "board?page=1";
	</c:if>
	
	$('.pagination a').click(function(){
		var page;
		var selectPage = $(this).text().trim();
		if(selectPage == '«'){   <%-- 시작페이지가 1인 경우 return--%>
			if(${pb.startPage} != '1'){
				page=${pb.startPage}-1;
				location.href = "board?page=" + page;
			}else{
				alert('이전페이지가 없습니다');
				return;	
			}
		}else if(selectPage == '»'){
			if(${pb.endPage} == ${pb.totalPage}){  <%--총페이지와 끝페이지가 다르면 return--%>
			return;
			}else{
			page=${pb.endPage}+1;
			location.href = "board?page=" + page;
			}
		}else{
		page = selectPage;
		location.href = "board?page=" + page;
		}
	});
	
	$('.pagination a').each(function(index, element) {
		if ($(element).text() == '${pb.currentPage}') {
			$(element).addClass('active');
		}
	});
	
	<!--검색창 -->	
	$('.searchbtn').click(function() {
		var data = $('.searchtext').val();
		if(data == ""){
			alert('검색내용이 없습니다.');
			return false;
		}else{
		location.href = "board?page=1&data="+data;
		}
	});
	
	<!-- 게시판 전체글로 가기 -->
	$('#hometext').click(function() {
		location.href = "board?page=1";
	});
});
	</script>
</body>
</html>
